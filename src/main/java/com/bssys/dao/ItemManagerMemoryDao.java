package com.bssys.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bssys.jaxb.model.Item;


@Component
public class ItemManagerMemoryDao implements ItemManagerDao {
	
	private List<Item> items = new ArrayList<Item>();

	@Override
	public Item getItem(String name) {
		// TODO Auto-generated method stub
		for (Item item: items){
			String key = item.getKey();
			if (key.equals(name)){
				return item;
			}
		}
		throw new RuntimeException("Item Not Found: " + name);
	}

	@Override
	public List<Item> getAllItems() {
		// TODO Auto-generated method stub
		return items;
	}

	@Override
	public void insertItem(Item item) {
		// TODO Auto-generated method stub
		items.add(item);
	}

	@Override
	public void updateItem(Item item) {
		// TODO Auto-generated method stub
		Item storeItem = getItem(item.getKey());
		storeItem.setValue(item.getValue());
	}

	@Override
	public void deleteItem(String name) {
		// TODO Auto-generated method stub
		Item storeItem = getItem(name);
		items.remove(storeItem);
	}
}
