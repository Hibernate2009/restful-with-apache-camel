package com.bssys.dao;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bssys.jaxb.model.Item;

@Service
public interface ItemManagerDao {
	public Item getItem(String name);
	public void deleteItem(String name);
	public List<Item> getAllItems();
	public void insertItem(Item item);
	public void updateItem(Item item);
}
