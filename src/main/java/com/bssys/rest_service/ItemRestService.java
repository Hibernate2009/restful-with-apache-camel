package com.bssys.rest_service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;

import com.bssys.dao.ItemManagerMemoryDao;
import com.bssys.jaxb.model.Item;
import com.bssys.jaxb.model.ItemRequest;
import com.bssys.jaxb.model.ItemResponse;
import com.bssys.service.ItemService;

@Path("/example")
public class ItemRestService implements ItemService{
	@Autowired
	private ItemManagerMemoryDao dao;

	@PUT
	@Path("addItem")
	@Consumes("application/json")
	@Produces("application/json")
	public ItemResponse addItem(ItemRequest request) {
		// TODO Auto-generated method stub
		ItemResponse userResponse = new ItemResponse();
		try{
			dao.insertItem(request.getItem());
			userResponse.setErrorMessage("Ok");
		}catch(Exception e){
			userResponse.setErrorMessage("Error: "+e.getMessage());
		}
		return userResponse;
	}

	@PUT
	@Path("updateItem")
	@Consumes("application/json")
	@Produces("application/json")
	public ItemResponse updateItem(ItemRequest request) {
		// TODO Auto-generated method stub
		ItemResponse userResponse = new ItemResponse();
		try{
			dao.updateItem(request.getItem());
			userResponse.setErrorMessage("Ok");
		}catch(Exception e){
			userResponse.setErrorMessage("Error: "+e.getMessage());
		}
		return userResponse;
	}

	@GET
	@Path("getItem/{key}")
	@Produces("application/json")
	public ItemResponse getItem(@PathParam("key") String key) {
		// TODO Auto-generated method stub
		ItemResponse userResponse = new ItemResponse();
		try{
			Item item = dao.getItem(key);
			userResponse.getItem().add(item);
			userResponse.setErrorMessage("Ok");
		}catch(Exception e){
			userResponse.setErrorMessage("Error: "+e.getMessage());
		}
		return userResponse;
	}

	@GET
	@Path("getAllItem")
	@Produces("application/json")
	public ItemResponse getAllItem() {
		// TODO Auto-generated method stub
		ItemResponse userResponse = new ItemResponse();
		try{
			List<Item> allItems = dao.getAllItems();
			List<Item> item = userResponse.getItem();
			item.addAll(allItems);
			userResponse.setErrorMessage("Ok");
		}catch(Exception e){
			userResponse.setErrorMessage("Error: "+e.getMessage());
		}
		return userResponse;
	}

	@DELETE
	@Path("removeItem/{key}")
	@Consumes("application/json")
	@Produces("application/json")
	public ItemResponse removeItem(@PathParam("key") String key) {
		// TODO Auto-generated method stub
		ItemResponse userResponse = new ItemResponse();
		try{
			dao.deleteItem(key);
			userResponse.setErrorMessage("Ok");
		}catch(Exception e){
			userResponse.setErrorMessage("Error: "+e.getMessage());
		}
		return userResponse;
	}

	@Override
	@GET
	@Path("stubRequest")
	@Produces("application/json")
	public ItemRequest stubRequest() {
		// TODO Auto-generated method stub
		ItemRequest itemRequest = new ItemRequest();
		Item item = new Item();
		item.setKey("key");
		item.setValue("value");
		itemRequest.setItem(item);
		return itemRequest;
	}
}
