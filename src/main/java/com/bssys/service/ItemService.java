package com.bssys.service;

import org.springframework.stereotype.Service;
import com.bssys.jaxb.model.ItemRequest;
import com.bssys.jaxb.model.ItemResponse;

@Service
public interface ItemService {
	public ItemResponse addItem(ItemRequest request);
	public ItemResponse updateItem(ItemRequest request);
	public ItemResponse getItem(String key);
	public ItemResponse getAllItem();
	public ItemResponse removeItem(String key);
	public ItemRequest stubRequest();
}
