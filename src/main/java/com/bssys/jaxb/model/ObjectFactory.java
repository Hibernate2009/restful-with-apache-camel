//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.03.13 at 01:17:13 PM GMT+04:00 
//


package com.bssys.jaxb.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bssys.jaxb.model package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ItemResponse_QNAME = new QName("http://bssys.com/item", "itemResponse");
    private final static QName _ItemRequest_QNAME = new QName("http://bssys.com/item", "itemRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bssys.jaxb.model
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Item }
     * 
     */
    public Item createItem() {
        return new Item();
    }

    /**
     * Create an instance of {@link ItemResponse }
     * 
     */
    public ItemResponse createItemResponse() {
        return new ItemResponse();
    }

    /**
     * Create an instance of {@link ItemRequest }
     * 
     */
    public ItemRequest createItemRequest() {
        return new ItemRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bssys.com/item", name = "itemResponse")
    public JAXBElement<ItemResponse> createItemResponse(ItemResponse value) {
        return new JAXBElement<ItemResponse>(_ItemResponse_QNAME, ItemResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bssys.com/item", name = "itemRequest")
    public JAXBElement<ItemRequest> createItemRequest(ItemRequest value) {
        return new JAXBElement<ItemRequest>(_ItemRequest_QNAME, ItemRequest.class, null, value);
    }

}
